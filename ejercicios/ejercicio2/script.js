	const os = require('os');
	var refresca = () => {

	var mlib=os.freemem()/1024/1024/1024;
	var tmem=os.totalmem()/1024/1024/1024;
	$('#totalmem').text(tmem.toFixed(2)  + " Gb");
	$('#mem').text(mlib.toFixed(2) + " Gb");
	$('#arch').text(os.arch());
	$('#cpus').text(os.cpus().length);
	$('#cpumodel').text(os.cpus()[0].model);
	$('#sisOp').text(os.platform());
	$('#ip').text(os.networkInterfaces().eth0[0].address);
	var user= os.userInfo().username;
	$('#usuario').text(user);
	var mins= os.uptime()/60;
	$('#tiempo').text(Math.round(mins) + " minutos");

	}
	setInterval(refresca, 1);