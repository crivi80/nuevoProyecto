
var express = require('express');
var app = express();
var mysql = require('mysql');
var fs = require('fs');
var conexion = {};
var bodyParser = require('body-parser');

fs.readFile('./conexion.json', (err,data) => {
  conexion = JSON.parse(data);  
  connection = mysql.createConnection(conexion);
  connection.connect();
});


app.use( bodyParser.json() );  
app.use( bodyParser.urlencoded({ extended: true }) ); 
app.use(express.static('images'));

app.post('/creaprofesor', function(req, res){

		connection.query('INSERT into profesores ( apellidos , nombre ) values ( ? , ? )' , [req.body.apellidos , req.body.nombre] , 
			function(error, results){
				
			
			
			res.redirect('/newprofesor');
			
		});
});

app.post('/editaprofesor', function(req, res){

		connection.query('UPDATE profesores SET apellidos = ?, nombre = ? where idprofesor = ?' , 
		[req.body.apellidos , req.body.nombre, req.body.idprofesor] , 
		function(error, results){
			
			
			res.redirect('/newprofesor');
			
		});
	});

app.post('/borraprofesor', function(req, res){

		connection.query('DELETE from profesores where idprofesor = ?' , 
		[req.body.idprofesor] , 
		function(error, results){
			
			
			res.redirect('/newprofesor');
			
		});

});

app.get('/profesores' , function (req , res){
	connection.query('SELECT * from profesores;', function (error, results){
		
		res.json(results);
		
	});
	
});

app.get('/newprofesor', (req, res) => {
  res.sendFile( __dirname + '/form.html');  
});

app.get('/form2', (req, res) => {
  res.sendFile( __dirname + '/form2.html');  
});

app.get('/profesor/:idprofesor', function(req, res){

	connection.query('SELECT * from profesores where idprofesor = ?' , [req.params.idprofesor] , function(error, results){
		res.json(results);
	});
});

app.get('/profesor/:nombre/:apellidos', function(req, res){

	connection.query('SELECT * from profesores where nombre = ? and apellidos = ?' , [req.params.nombre ,
		 req.params.apellidos] , function(error, results){
		res.json(results);
	});
});

app.get('/profesornombre/:nombre', function(req, res){

	connection.query('SELECT * from profesores where nombre = ?' , [req.params.nombre] , function(error, results){
		res.json(results);
	});
});

app.get('/profesorapellidos/:apellidos', function(req, res){

	connection.query('SELECT * from profesores where apellidos = ?' , [req.params.apellidos] , function(error, results){
		res.json(results);
	});
});


app.get('/profesores/describe', function(req, res){

	connection.query('describe profesores' , function(error, results){
		res.json(results);
	});
});

app.get('/tablas', function(req, res){

	connection.query('show tables' , function(error, results){
		res.json(results);
	});
});

app.get('/', function (req, res) {
	
  res.sendFile( __dirname + '/form.html'); 
});

app.get('/profesores/apellidos' , function (req , res){
	connection.query('SELECT * from profesores order by apellidos', function (error, results){
		
		res.json(results);
		
	});
	
});
app.get('/profesores/id' , function (req , res){
	connection.query('SELECT * from profesores order by idprofesor', function (error, results){
		
		res.json(results);
		
	});
	
});
app.get('/profesores/nombre' , function (req , res){
	connection.query('SELECT * from profesores order by nombre', function (error, results){
		
		res.json(results);
		
	});
	
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000! OUUUUUUUHHH YEAHHHHH');
});
