var database = "http://192.168.100.71:5984/ricardo/";
var foto='{}';
var adjunto={};
var src='';
var iddoc='';
var revdoc='';
var tipoDoc='';
var cargador = new FileReader()	;
var contenido = '';
var documento = '';


function getAlumnos(){

	$('#listado').html("");
	
	$.ajax({

		type: "get",
	  	url: database + "_all_docs?include_docs=true",
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

	  }).done(function (data) { 
		
		$.each(data.rows, function (indice,valor){

			$('#listado').append(`<li class="lis" onclick="cambiaListado('${valor.doc._id}')">
				${valor.doc.apellidos}, ${valor.doc.nombre}</li>`);
			});
		});
}

function cambiaListado(id){

	$.ajax({

		type: "get",
	  	url: database + id,
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

		}).done(function(data){
			console.log(data);
			
			$('#apellidos').val(data.apellidos);
			$('#nombre').val(data.nombre);
			$('#telefono').val(data.telefono);
			$('#id').val(data._id);
			$('#rev').val(data._rev);
			$('#tipoDoc').val(data.tipoDoc);
			$('#attachments').val(data.attachments);
			$('#src').val(data.src);
			$('#imagen').attr("src", `${database}${id}/${data.src}`);
			adjunto=data._attachments;
			src=data.src;
			iddoc=data._id;
			revdoc=data._rev;
		})
}

function modificar(){
	
	let doc = {
		_id: iddoc,
		_rev: revdoc,
		apellidos: $('#apellidos').val(),
		nombre: $('#nombre').val(),
		telefono: $('#telefono').val(),
		tipoDoc: tipoDoc,
		_attachments: adjunto,
		src: src
	}
	console.log(doc);
	
	$.ajax({

		type: "post",
	  	url: database,
	  	data: JSON.stringify(doc),
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

	}).done(function(data){
		console.log(data);
		$('#listado').html("");
		getAlumnos();
		

	})
}

function crear(){

	let crea = {
		
		apellidos: $('#apellidos').val(),
		nombre: $('#nombre').val(),
		telefono: $('#telefono').val(),
		

		}
	
	$.ajax({

		type: "post",
	  	url: database, 
	  	data: JSON.stringify(crea),
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

	}).done(function(data){
		$('#listado').html("");
		getAlumnos();
	})
}

function borrar(){
		
	$.ajax({

		type: "delete",
	  	url: database + $('#id').val() + "?rev=" + $('#rev').val(),	  
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

	}).done(function(data){
		$('#listado').html("");
		getAlumnos();

	})
}

function reset(){
		$('#id').val("");
		$('#rev').val("");
		$('#tipoDoc').val("");
		$('#attachments').val("");
		$('#src').val("");
		$('#apellidos').val("");
		$('#nombre').val("");
		$('#telefono').val("");
		$('#oculto').css("display", "none");
		iddoc='';
		revdoc='';
		foto='{}';
		src='';
		tipoDoc='';
		adjunto={};

}

function ordenarPorApellidos(){

	$.ajax({
		type: "get",
	  	url: database + "_design/view/_view/ordenaApellidos",
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"


	}).done(function(data){
			
			$('#listadoApellidos').html("");
			$.each(data.rows, function (indice,valor){


			$('#listadoApellidos').append(`<li class="lis" onclick="cambiaListado('${valor.id}')">${valor.key}, ${valor.value}</li>`);
			$('#oculto').css("display", "block");
			});

		});
}

function changeFile(archivo){	

	cargador.onloadend = function () {	

		contenido = cargador.result.split('base64,')[1]	;
		let nombre = $('#nombre').val();
		let apellidos = $('#apellidos').val();
		let telefono = $('#telefono').val();
		
		
		documento = 	`{
		
		"apellidos": "${apellidos}",
		"nombre": "${nombre}",
		"telefono": "${telefono}",
		"tipoDoc": "adjuntos",
		"_attachments": {
					"${archivo.name}": {			
					  "content_type": "${archivo.type}",	
					  "data": "${contenido}"
					}
				},
		"src": "${archivo.name}" }` ;
		foto=`{
					"${archivo.name}": {			
					  "content_type": "${archivo.type}",	
					  "data": "${contenido}"
					}`;
		src=archivo.name;
		tipoDoc="adjuntos";
		adjunto=JSON.parse(foto);

			
  }  
	cargador.readAsDataURL(archivo);	
	console.log(documento);
	

}

	

function graba(documento){	
	

	
	console.log(documento);

	$.ajax({
		url: database,
		type: 'post',	
		contentType: 'application/json',
		dataType: "json",
		data: documento
	})
	.done( function(respuesta){ 
			console.log(documento);
			$('#listado').html("");
			getAlumnos();



		});	
} 
/*
$ curl -vX PUT http://127.0.0.1:5984/my_database/001/boy.jpg?rev=1-
967a00dff5e02add41819138abb3284d --data-binary @boy.jpg -H "ContentType:
image/jpg"
*/

$(document).ready( function(){
	getAlumnos();
});


