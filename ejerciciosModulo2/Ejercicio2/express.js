var express = require('express');
var app = express();
var mysql = require('mysql');
var fs = require('fs');
var conexion = {};
var bodyParser = require('body-parser');

fs.readFile('./conexion.json', (err,data) => {
  conexion = JSON.parse(data);  
  connection = mysql.createConnection(conexion);
  connection.connect();
});


app.use( bodyParser.json() );  
app.use( bodyParser.urlencoded({ extended: true }) ); 


app.get('/profesores' , function (req , res){
	connection.query('SELECT * from profesores;', function (error, results){
		
		res.json(results);
		
	});
	
});

app.get('/profesor/:idprofesor', function(req, res){

	connection.query('SELECT * from profesores where idprofesor = ?' , [req.params.idprofesor] , function(error, results){
		res.json(results);
	});
});

app.get('/profesor/:nombre/:apellidos', function(req, res){

	connection.query('SELECT * from profesores where nombre = ? and apellidos = ?' , [req.params.nombre ,
		 req.params.apellidos] , function(error, results){
		res.json(results);
	});
});

app.get('/profesornombre/:nombre', function(req, res){

	connection.query('SELECT * from profesores where nombre = ?' , [req.params.nombre] , function(error, results){
		res.json(results);
	});
});

app.get('/profesorapellidos/:apellidos', function(req, res){

	connection.query('SELECT * from profesores where apellidos = ?' , [req.params.apellidos] , function(error, results){
		res.json(results);
	});
});


app.get('/profesores/describe', function(req, res){

	connection.query('describe profesores' , function(error, results){
		res.json(results);
	});
});

app.get('/tablas', function(req, res){

	connection.query('show tables' , function(error, results){
		res.json(results);
	});
});

app.get('/profesores/apellidos' , function (req , res){
	connection.query('SELECT * from profesores order by apellidos', function (error, results){
		
		res.json(results);
		
	});
	
});
app.get('/profesores/id' , function (req , res){
	connection.query('SELECT * from profesores order by idprofesor', function (error, results){
		
		res.json(results);
		
	});
	
});
app.get('/profesores/nombre' , function (req , res){
	connection.query('SELECT * from profesores order by nombre', function (error, results){
		
		res.json(results);
		
	});
	
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000! OUUUUUUUHHH YEAHHHHH');
});