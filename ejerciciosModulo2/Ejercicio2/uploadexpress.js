const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
 
// default options 
app.use(fileUpload());
 
app.post('/upload', function(req, res) {
  if (!req.files)
    return res.status(400).send('No files were uploaded.');
 
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file 
  let sampleFile = req.files.sampleFile;
 
  // Use the mv() method to place the file somewhere on your server 
  sampleFile.mv('/somewhere/on/your/server/filename.jpg', function(err) {
    if (err)
      return res.status(500).send(err);
 
    res.send('File uploaded!');
  });
});  
/* <html>
  <body>
    <form ref='uploadForm' 
      id='uploadForm' 
      action='http://localhost:8000/upload' 
      method='post' 
      encType="multipart/form-data">
        <input type="file" name="sampleFile" />
        <input type='submit' value='Upload!' />
    </form>     
  </body>
</html> 


var lector = new FileReader() 
var contenido = ''       
var foto = ''

function fileChange(fichero){

  console.log(fichero)

  lector.onloadend = function(){
 
          
    $('#img1').attr('src' , lector.result)
    contenido = lector.result.split('base64,')[1] 
    foto = `{
              "${fichero.name}":{
                "content_type":"${fichero.type}",
                "data": "${contenido}"
      }
    }

    `
  
}
    lector.readAsDataURL(fichero)
  
  }



*/ 