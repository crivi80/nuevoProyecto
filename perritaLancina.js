var database = "http://192.168.100.76:5984/ricardo/";

function getAlumnos(){

	$('#listado').html("");
	
	$.ajax({

		type: "get",
	  	url: database + "_all_docs?include_docs=true",
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

	  }).done(function (data) { 
		
		$.each(data.rows, function (indice,valor){

			$('#listado').append(`<li class="lis" onclick="cambiaListado('${valor.doc._id}')">
				${valor.doc.apellidos}, ${valor.doc.nombre}</li>`);
			});
		});
}

function cambiaListado(id){

	$.ajax({

		type: "get",
	  	url: database + id,
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

		}).done(function(data){
			
			$('#apellidos').val(data.apellidos);
			$('#nombre').val(data.nombre);
			$('#telefono').val(data.telefono);
			$('#id').val(data._id);
			$('#rev').val(data._rev);
		})
}

function modificar(){
	let doc = {
		_id: $('#id').val(),
		_rev: $('#rev').val(),
		apellidos: $('#apellidos').val(),
		nombre: $('#nombre').val(),
		telefono: $('#telefono').val()
	}
	
	$.ajax({

		type: "post",
	  	url: database,
	  	data: JSON.stringify(doc),
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

	}).done(function(data){
		console.log(data);
		//$('#selector').html("");
		$('#listado').html("");
		getAlumnos();
		

	})
}

function crear(){

	let crea = {
		
		apellidos: $('#apellidos').val(),
		nombre: $('#nombre').val(),
		telefono: $('#telefono').val()

		}
	
	$.ajax({

		type: "post",
	  	url: database, 
	  	data: JSON.stringify(crea),
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

	}).done(function(data){
		$('#listado').html("");
		getAlumnos();
	})
}

function borrar(){
		
	$.ajax({

		type: "delete",
	  	url: database + $('#id').val() + "?rev=" + $('#rev').val(),	  
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"

	}).done(function(data){
		$('#listado').html("");
		getAlumnos();

	})
}

function reset(){
		$('#id').val("");
		$('#rev').val("");

		$('#apellidos').val("");
		$('#nombre').val("");
		$('#telefono').val("");
		$('#oculto').css("display", "none");

}

function ordenarPorApellidos(){

	$.ajax({
		type: "get",
	  	url: database + "_design/view/_view/ordenaApellidos",
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8"


	}).done(function(data){
			
			$('#listadoApellidos').html("");
			$.each(data.rows, function (indice,valor){

			console.log(valor.key, valor.value);

			$('#listadoApellidos').append(`<li class="lis" onclick="cambiaListado('${valor.id}')">${valor.key}, ${valor.value}</li>`);
			$('#oculto').css("display", "block");
			});

		});
}

var cargador = new FileReader()	
var contenido = ''
var stringObjeto = ''
var nomArchivo =''

function changeFile(archivo){	

	cargador.onloadend = function () {	

		contenido = cargador.result.split('base64,')[1]		
		stringObjeto = `
			{
				"tipoDoc": "adjuntos",
				"_attachments": {
					"${archivo.name}": {			
					  "content_type": "${archivo.type}",	
					  "data": "${contenido}"
					}
				}
			}
			`	
			nomArchivo = 	

				`"tipoDoc": "adjuntos",
				"_attachments": {
					"${archivo.name}": {			
					  "content_type": "${archivo.type}",	
					  "data": "${contenido}"
					}
				}`
			
  }  
	cargador.readAsDataURL(archivo)	
}

	var id = $('#id').val();
	var rev = $('#rev').val();
	var nombre = $('#nombre').val()
	var apellidos = $('#apellidos').val()
	var telefono = $('#telefono').val()

	var documento = `{
		_id: ${id},
		_rev: ${rev},
		apellidos: ${apellidos},
		nombre: ${nombre},
		telefono: ${telefono},
		${stringObjeto}
		}`

function graba(documento){	
	
	
	console.log(nomArchivo);
	console.log(documento);

	$.ajax({
		url: database,
		type: 'post',	
		contentType: 'application/json',
		dataType: "json",
		data: JSON.stringify(documento)
	})
	.done( function(respuesta){ 

			
			$('#listado').html("");
			getAlumnos();



		})	
}
/*
$ curl -vX PUT http://127.0.0.1:5984/my_database/001/boy.jpg?rev=1-
967a00dff5e02add41819138abb3284d --data-binary @boy.jpg -H "ContentType:
image/jpg"
*/

$(document).ready( function(){
	getAlumnos();
});


